<?php

namespace Bundle\GroupBuyBundle\Document;

class Rule {

	const TYPE_PERCENT = 'percent';
	const TYPE_REDUCED = 'reduced';

	protected $type;
	protected $apply;
	protected $qtyStart;
	protected $qtyThreshold;
	protected $qtyMaxSold;

	static protected $typeChoices = array(
		self::TYPE_PERCENT => 'Percent Off',
		self::TYPE_REDUCED => 'Reduced',
	);

	static protected $typeSymbols = array(
		self::TYPE_PERCENT => '%',
		self::TYPE_REDUCED => '$',
	);

	static public function getTypeSymbol($type = null) {
		if (isset(self::$typeSymbols[$type])) {
			return self::$typeSymbols[$type];
		}
	}

	static public function getTypeChoices() {
		return self::$typeChoices;
	}

	public function setData(array $data = array()) {
		foreach ($data as $key => $val) {
			$this->{$key} = $val;
		}
	}

	public function getData() {
		return array(
			'apply' => $this->apply,
			'qtyMaxSold' => $this->qtyMaxSold,
			'qtyStart' => $this->qtyStart,
			'qtyThreshold' => $this->qtyThreshold,
			'type' => $this->type,
		);
	}

	public function getTypeText() {
		$type = $this->getType();
		if (isset(self::$typeChoices[$type])) {
			return self::$typeChoices[$type];
		}
	}

	public function setType($type) {
		$type = (string) $type;
		if (FALSE === $this->isTypeValid($type)) {
			throw new \InvalidArgumentException($type.' is not a valid Rule Type.');
		}
		return $this->type = $type;
	}

	public function getType() {
		return $this->type;
	}

	public static function isTypeValid($type) {
		return array_key_exists($type, static::$typeChoices);
	}

	public function setApply($apply) {
		return $this->apply = (string) $apply;
	}

	public function getApply() {
		return $this->apply;
	}

	public function setQtyThreshold($qty) {
		$this->qtyThreshold = (int) $qty;
	}

	public function getQtyThreshold() {
		return $this->qtyThreshold;
	}

	public function setQtyStart($qty) {
		$this->qtyStart = (int) $qty;
	}

	public function getQtyStart() {
		return $this->qtyStart;
	}

	public function setQtyMaxSold($qty) {
		$this->qtyMaxSold = (int) $qty;
	}

	public function getQtyMaxSold() {
		return $this->qtyMaxSold;
	}

}

<?php

namespace Bundle\GroupBuyBundle\Document;

use DoctrineExtensions\Sluggable\Sluggable;

class Deal implements Sluggable {

	protected $demo;
	protected $description;
	protected $id;	
	protected $meta = array();
	protected $price;
	protected $qtySold;
	protected $region = array();
	protected $releaseTime;
	protected $rule;
	protected $slug;
	protected $shippingCost;
	protected $timeEnd;
	protected $timeExpire;
	protected $timeStart;
	protected $title;

	public function setDemo($demo) {
		$this->demo = (bool) $demo;
	}

	public function getDemo() {
		return $this->demo;
	}

	public function isDemo() {
		return (isset($this->demo))? $this->demo : FALSE;
	}	

	public function setDescription($d) {
		$this->description = (string) $d;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getId() {
		return $this->id;
	}

	public function setMeta(array $m) {
		return $this->meta = $m;
	}

	public function getMeta() {
		return $this->meta;
	}

	public function setMetaTitle($t) {
		$this->meta['title'] = strip_tags((string) $t);
	}

	public function getMetaTitle() {
		if (array_key_exists('title', $this->meta)) {
			return $this->meta['title'];
		}
	}

	public function setMetaDescription($d) {
		$this->meta['description'] = strip_tags((string) $d);
	}

	public function getMetaDescription() {
		if (array_key_exists('description', $this->meta)) {
			return $this->meta['description'];
		}
	}

	public function setMetaKeywords($k) {
		$this->meta['keywords'] = strip_tags((string) $k);
	}

	public function getMetaKeywords() {
		if (array_key_exists('keywords', $this->meta)) {
			return $this->meta['keywords'];
		}
	}

	public function setMetaRobots($r) {
		$this->meta['robots'] = (string) $r;
	}

	public function getMetaRobots() {
		if (array_key_exists('robots', $this->meta)) {
			return $this->meta['robots'];
		}
	}

	public function setPrice($price) {
		$this->price = (double) $price;
	}

	public function getPrice() {
		return $this->price;
	}

	public function setQtySold($qty) {
		$this->qtySold = (int) $qty;
	}

	public function getQtySold() {
		return $this->qtySold;
	}

	public function setRegion(array $region = array()) {
		$this->region = $region;
	}

	public function getRegion() {
		return $this->region;
	}

	public function setReleaseTime(\DateTime $time) {
		$this->releaseTime = $time;
	}

	public function getReleaseTime() {
		return $this->releaseTime;
	}

	public function setRule(\Bundle\GroupBuyBundle\Document\Rule $rule) {
		$this->rule = $rule;
	}

	public function getRule() {
		return $this->rule;
	}

	public function setRuleData(array $data = array()) {
		if (!isset($this->rule)) {
			$this->rule = new Rule();
		}
		$this->rule->setData($data);
	}

	public function getRuleData() {
		if (!isset($this->rule)) {
			$this->rule = new Rule();
		}
		return $this->rule->getData();
	}

	public function setShippingCost($cost) {
		$this->shippingCost = (double) $cost;
	}

	public function getShippingCost() {
		return $this->shippingCost;
	}

	public function setSlug($slug) {
		$this->slug = (string) $slug;
	}

	public function getSlug() {
		return $this->slug;
	}
	
	public function getSlugFieldName() {
		return 'slug';
	}

	public function getSlugGeneratorFields() {
		return array('title');
	}

	public function setTimeEnd(\DateTime $time) {
		$this->timeEnd = $time;
	}

	public function getTimeEnd() {
		return $this->timeEnd;
	}

	public function setTimeExpire(\DateTime $time) {
		$this->timeExpire = $time;
	}

	public function getTimeExpire() {
		return $this->timeExpire;
	}

	public function setTimeStart(\DateTime $time) {
		$this->timeStart = $time;
	}

	public function getTimeStart() {
		return $this->timeStart;
	}

	public function setTitle($title) {
		$this->title = (string) $title;
	}

	public function getTitle() {
		return $this->title;
	}

}

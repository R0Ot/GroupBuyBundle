<?php

namespace Bundle\GroupBuyBundle\Service;

use Bundle\GroupBuyBundle\Document\Deal;
use Bundle\GroupBuyBundle\Document\Rule;

class Hustler {
	
	protected $deal;
	protected $qtyRequested;
	protected $timeCurrent;
	protected $floor = TRUE;

	public function __construct(Deal $deal = null, \DateTime $now = null) {
		$this->deal = $deal;
		$this->timeCurrent = ($now) ? $now : new \DateTime();
	}

	public function setDeal(Deal $deal) {
		$this->deal = $deal;
	}

	public function getDeal() {
		return $this->deal;
	}

	public function setTimeCurrent(\DateTime $date) {
		$this->timeCurrent = $date;
	}

	public function getTimeCurrent() {
		return $this->timeCurrent;
	}

	public function isBeforeStartTime() {
		if ($this->getDeal()->getTimeStart() > $this->getTimeCurrent()) {
			return TRUE;
		}
		return FALSE;
	}

	public function isAfterEndTime() {
		if ($this->getDeal()->getTimeEnd() < $this->getTimeCurrent()) {
			return TRUE;
		}
		return FALSE;
	}

	public function isAvailableTime() {
		if ($this->getDeal()->getTimeStart() > $this->getTimeCurrent()) {
			return FALSE;
		}
		if ($this->getDeal()->getTimeEnd() < $this->getTimeCurrent()) {
			return FALSE;
		}
		return TRUE;
	}

	public function isExpired() {
		if ($this->getDeal()->getTimeExpire() < $this->getTimeCurrent()) {
			return TRUE;
		}
		return FALSE;
	}

	public function getSecondsRemaining() {
		return $this->getDeal()->getTimeEnd()->getTimestamp() - $this->getTimeCurrent()->getTimestamp();	
	}

	public function getMillisecondsRemaining() {
		return $this->getSecondsRemaining() * 1000;
	}

	public function isInStock() {
		if ($this->getStockRemaining() > 0) {
			return TRUE;
		}
		return FALSE;
	}

	public function setQtyRequested($qty) {
		$this->qtyRequested = (int) $qty;
	}

	public function getQtyRequested() {
		return $this->qtyRequested;
	}

	public function isThresholdMet() {
		if ($this->getDeal()->getRule()->getQtyThreshold() <= $this->getDeal()->getQtySold()) {
			return TRUE;
		}
		return FALSE;
	}

	public function getStockRemaining() {
		return $this->getDeal()->getRule()->getQtyMaxSold() - $this->getDeal()->getQtySold();		
	}

	public function isQtyRequestedInStock() {
		if (!$this->isInStock()) {
			return FALSE;
		}
		if ($this->getStockRemaining() < $this->getQtyRequested()) {
			return FALSE;
		}
		return TRUE;
	}

	public function isPurchasable() {
		if (!$this->isAvailableTime()) {
			return FALSE;
		}
		if (!$this->isQtyRequestedInStock()) {
			return FALSE;
		}
		return TRUE;
	}

	public function getDealPrice() {
		$price = $this->getDeal()->getPrice();
		$ruleType = $this->getDeal()->getRule()->getType();
		$apply = $this->getDeal()->getRule()->getApply();
		$percent = Rule::TYPE_PERCENT;
		switch ($ruleType) {
			case $percent:
				$dealPrice = $price - ($price * ($apply / 100));
			break;
			default:
				$dealPrice = $price - $apply;
		}
		return $dealPrice;
	}

	public function getSavingsPercent() {
		$price = $this->getDeal()->getPrice();
		$ruleType = $this->getDeal()->getRule()->getType();
		$apply = $this->getDeal()->getRule()->getApply();
		$percent = Rule::TYPE_PERCENT;
		switch ($ruleType) {
			case $percent:
				$savings = $apply / 100;
			break;
			default:
				$savings = $apply / $price;
		}
		return $savings;
	}

	public function getSavingsReduced() {
		$price = $this->getDeal()->getPrice();
		$ruleType = $this->getDeal()->getRule()->getType();
		$apply = $this->getDeal()->getRule()->getApply();
		$percent = Rule::TYPE_PERCENT;
		switch ($ruleType) {
			case $percent:
				$savings = ($apply / 100) * $price;
			break;
			default:
				$savings = $apply;
		}
		return $savings;
	}

	public function formatPercent($value) {
		$absolute = abs($value) * 100;
		$transformed = $absolute;
		if ($this->floor) {
			$transformed = floor($absolute);
		}
		$prefix = '';
		if ($value < 0) {
			$prefix = '-';
		}
		$formatted  = $prefix;
		$formatted .= $transformed;
		$formatted .= Rule::getTypeSymbol(Rule::TYPE_PERCENT);
		return $formatted;
	}

	public function formatPrice($value) {
		$absolute = abs($value);
		$transformed = $absolute;
		if ($this->floor) {
			$transformed = floor($absolute);
		}
		$prefix = '';
		if ($value < 0) {
			$prefix = '-';
		}
		$formatted  = $prefix;
		$formatted .= Rule::getTypeSymbol(Rule::TYPE_REDUCED);
		$formatted .= $transformed;
		return $formatted;
	}

}

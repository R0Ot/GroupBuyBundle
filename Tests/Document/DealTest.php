<?php

namespace Bundle\GroupBuyBundle\Tests;

use Bundle\GroupBuyBundle\Document\Deal;

class DealTest extends \PHPUnit_Framework_TestCase {

	protected $deal;

	public function setUp() {
		parent::setUp();
		$this->deal = new Deal();
	}

	public function tearDown() {
		unset($this->deal);
	}

	public function testConstructor() {
		$this->assertNull($this->deal->getId());
	}
	
	public function testSetGetTitle() {
		$this->assertNull($this->deal->getTitle());
		
		$title1 = "Test Deal 1";
		$this->deal->setTitle($title1);
		$this->assertEquals($title1, $this->deal->getTitle());
	
		$title2 = 100;
		$this->deal->setTitle($title2);
		$this->assertEquals('100', $this->deal->getTitle());

	}

	public function testSetGetDescription() {
		$this->assertNull($this->deal->getDescription());
		
		$desc1 = "Test Desc 1";
		$this->deal->setDescription($desc1);
		$this->assertEquals($desc1, $this->deal->getDescription());
	
		$desc2 = 100;
		$this->deal->setDescription($desc2);
		$this->assertEquals('100', $this->deal->getDescription());

	}

	public function testSetGetReleaseTime() {
		$this->assertNull($this->deal->getReleaseTime());
		
		$time1 = new \DateTime("2010-01-01");
		$this->deal->setReleaseTime($time1);
		$this->assertEquals($time1, $this->deal->getReleaseTime());
	
		$time2 = new \DateTime("2010-01-02");
		$this->deal->setReleaseTime($time2);
		$this->assertEquals($time2, $this->deal->getReleaseTime());

	}
	
	public function testSetGetTimeStart() {
		$this->assertNull($this->deal->getTimeStart());
		
		$time1 = new \DateTime("2010-01-01");
		$this->deal->setTimeStart($time1);
		$this->assertEquals($time1, $this->deal->getTimeStart());
	
		$time2 = new \DateTime("2010-01-02");
		$this->deal->setTimeStart($time2);
		$this->assertEquals($time2, $this->deal->getTimeStart());

	}
	
	public function testSetGetTimeEnd() {
		$this->assertNull($this->deal->getTimeEnd());
		
		$time1 = new \DateTime("2010-01-01");
		$this->deal->setTimeEnd($time1);
		$this->assertEquals($time1, $this->deal->getTimeEnd());
	
		$time2 = new \DateTime("2010-01-02");
		$this->deal->setTimeEnd($time2);
		$this->assertEquals($time2, $this->deal->getTimeEnd());

	}
	
	public function testSetGetTimeExpire() {
		$this->assertNull($this->deal->getTimeExpire());
		
		$time1 = new \DateTime("2010-01-01");
		$this->deal->setTimeExpire($time1);
		$this->assertEquals($time1, $this->deal->getTimeExpire());
	
		$time2 = new \DateTime("2010-01-02");
		$this->deal->setTimeExpire($time2);
		$this->assertEquals($time2, $this->deal->getTimeExpire());

	}
	
	public function testSetGetQtySold() {
		$this->assertNull($this->deal->getQtySold());
		
		$qty1 = 1;
		$this->deal->setQtySold($qty1);
		$this->assertEquals($qty1, $this->deal->getQtySold());
	
		$qty2 = 10;
		$this->deal->setQtySold($qty2);
		$this->assertEquals($qty2, $this->deal->getQtySold());

	}
	
	public function testSetGetShippingCost() {
		$this->assertNull($this->deal->getShippingCost());
		
		$price1 = 1;
		$this->deal->setShippingCost($price1);
		$this->assertEquals($price1, $this->deal->getShippingCost());
	
		$price2 = 10;
		$this->deal->setShippingCost($price2);
		$this->assertEquals($price2, $this->deal->getShippingCost());

	}
	
	public function testSetGetPrice() {
		$this->assertNull($this->deal->getPrice());
		
		$price1 = 1;
		$this->deal->setPrice($price1);
		$this->assertEquals($price1, $this->deal->getPrice());
	
		$price2 = 10;
		$this->deal->setPrice($price2);
		$this->assertEquals($price2, $this->deal->getPrice());

	}

	public function testSetGetMetaTitle() {
		$this->assertNull($this->deal->getMetaTitle());
		
		$title1 = "Test Meta Post 1";
		$this->deal->setMetaTitle($title1);
		$this->assertEquals($title1, $this->deal->getMetaTitle());
	
		$title2 = 100;
		$this->deal->setMetaTitle($title2);
		$this->assertEquals('100', $this->deal->getMetaTitle());

	}

	public function testSetGetMetaDescription() {
		$this->assertNull($this->deal->getMetaDescription());
		
		$desc1 = "Test Meta Desc 1";
		$this->deal->setMetaDescription($desc1);
		$this->assertEquals($desc1, $this->deal->getMetaDescription());
	
		$desc2 = 100;
		$this->deal->setMetaDescription($desc2);
		$this->assertEquals('100', $this->deal->getMetaDescription());

	}

	public function testSetGetMetaKeywords() {
		$this->assertNull($this->deal->getMetaKeywords());
		
		$keywords1 = "Test Meta Keywords 1";
		$this->deal->setMetaKeywords($keywords1);
		$this->assertEquals($keywords1, $this->deal->getMetaKeywords());
	
		$keywords2 = 100;
		$this->deal->setMetaKeywords($keywords2);
		$this->assertEquals('100', $this->deal->getMetaKeywords());

	}

}

<?php

namespace Bundle\CampaignBundle\Tests;

use Bundle\CampaignBundle\Document\Rule;

class RuleTest extends \PHPUnit_Framework_TestCase {

	protected $rule;

	public function setUp() {
		parent::setUp();
		$this->rule = new Rule();
	}

	public function tearDown() {
		unset($this->rule);
	}

	public function testSetGetType() {
		$this->assertNull($this->rule->getType());
		
		$type1 = Rule::TYPE_PERCENT;
		$this->rule->setType($type1);
		$this->assertEquals($type1, $this->rule->getType());
	
		$type2 = Rule::TYPE_REDUCED;
		$this->rule->setType($type2);
		$this->assertEquals($type2, $this->rule->getType());
	}

	public function testSetGetApply() {
		$this->assertNull($this->rule->getApply());
		
		$val1 = -1;
		$this->rule->setApply($val1);
		$this->assertEquals($val1, $this->rule->getApply());
	
		$val2 = '0.10';
		$this->rule->setApply($val2);
		$this->assertEquals($val2, $this->rule->getApply());
	}

	public function testSetGetQtyThreshold() {
		$this->assertNull($this->rule->getQtyThreshold());
		
		$qty1 = 1;
		$this->rule->setQtyThreshold($qty1);
		$this->assertEquals($qty1, $this->rule->getQtyThreshold());
	
		$qty2 = 10;
		$this->rule->setQtyThreshold($qty2);
		$this->assertEquals($qty2, $this->rule->getQtyThreshold());
	}
	
	public function testSetGetQtyMaxSold() {
		$this->assertNull($this->rule->getQtyMaxSold());
		
		$qty1 = 1;
		$this->rule->setQtyMaxSold($qty1);
		$this->assertEquals($qty1, $this->rule->getQtyMaxSold());
	
		$qty2 = 10;
		$this->rule->setQtyMaxSold($qty2);
		$this->assertEquals($qty2, $this->rule->getQtyMaxSold());
	}

	public function testSetGetQtyStart() {
		$this->assertNull($this->rule->getQtyStart());
		
		$qty1 = 1;
		$this->rule->setQtyStart($qty1);
		$this->assertEquals($qty1, $this->rule->getQtyStart());
	
		$qty2 = 10;
		$this->rule->setQtyStart($qty2);
		$this->assertEquals($qty2, $this->rule->getQtyStart());
	}

}

<?php

namespace Bundle\GroupBuyBundle\Tests;

use Bundle\GroupBuyBundle\Document\Deal;
use Bundle\GroupBuyBundle\Document\Rule;
use Bundle\GroupBuyBundle\Hustler;

class HustlerTest extends \PHPUnit_Framework_TestCase {

	protected $deal;
	protected $hustler;
	protected $rule;

	public function setUp() {
		parent::setUp();
		$this->deal = new Deal();
		$this->rule = new Rule();
		$this->hustler = new Hustler();
	}

	public function tearDown() {
		unset($this->deal);
		unset($this->rule);
		unset($this->hustler);
	}

	public function testConstructor() {
		$hustler1 = new Hustler();
		$this->assertNull($hustler1->getDeal());
		$this->assertNotNull($hustler1->getTimeCurrent());
		$deal2 = new Deal();
		$hustler2 = new Hustler($deal2);
		$this->assertEquals($deal2, $hustler2->getDeal());
		$deal3 = new Deal();
		$now3 = new \DateTime('2010-01-01');
		$hustler3 = new Hustler($deal3, $now3);
		$this->assertEquals($deal3, $hustler3->getDeal());
		$this->assertEquals($now3, $hustler3->getTimeCurrent());
	}

	public function testGetSetDeal() {
		$this->assertNull($this->hustler->getDeal());
		$this->hustler->setDeal($this->deal);
		$this->assertEquals($this->deal, $this->hustler->getDeal());
	}

	public function testGetSetTimeCurrent() {
		$this->assertEquals(new \DateTime(), $this->hustler->getTimeCurrent());
		$time = new \DateTime('2010-01-01');
		$this->hustler->setTimeCurrent($time);
		$this->assertEquals($time, $this->hustler->getTimeCurrent());
	}

	public function testSetGetQtyRequested() {
		$this->assertNull($this->hustler->getQtyRequested());
		$qty1 = 1;
		$this->hustler->setQtyRequested($qty1);
		$this->assertEquals($qty1, $this->hustler->getQtyRequested());
		$qty2 = 10;
		$this->hustler->setQtyRequested($qty2);
		$this->assertEquals($qty2, $this->hustler->getQtyRequested());
	}

	public function testIsAvailableTime() {
		$this->deal->setTimeStart(new \DateTime('2010-01-01 02:00:00'));
		$this->deal->setTimeEnd(new \DateTime('2010-01-01 03:00:00'));
		$this->hustler->setDeal($this->deal);
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 02:30:00'));
		$this->assertTrue($this->hustler->isAvailableTime());
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 02:00:00'));
		$this->assertTrue($this->hustler->isAvailableTime());
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 03:00:00'));
		$this->assertTrue($this->hustler->isAvailableTime());
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 01:59:59'));
		$this->assertFalse($this->hustler->isAvailableTime());
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 03:00:01'));
		$this->assertFalse($this->hustler->isAvailableTime());
	}

	public function testIsExpired() {
		$this->deal->setTimeExpire(new \DateTime('2010-01-01 04:00:00'));
		$this->hustler->setDeal($this->deal);
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 03:59:59'));
		$this->assertFalse($this->hustler->isExpired());
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 04:00:00'));
		$this->assertFalse($this->hustler->isExpired());
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 04:00:01'));
		$this->assertTrue($this->hustler->isExpired());
	}

	public function testIsBeforeStartTime() {
		$this->deal->setTimeStart(new \DateTime('2010-01-01 04:00:00'));
		$this->hustler->setDeal($this->deal);
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 03:59:59'));
		$this->assertTrue($this->hustler->isBeforeStartTime());
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 04:00:00'));
		$this->assertFalse($this->hustler->isBeforeStartTime());
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 04:00:01'));
		$this->assertFalse($this->hustler->isBeforeStartTime());
	}

	public function testIsAfterEndTime() {
		$this->deal->setTimeEnd(new \DateTime('2010-01-01 04:00:00'));
		$this->hustler->setDeal($this->deal);
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 03:59:59'));
		$this->assertFalse($this->hustler->isAfterEndTime());
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 04:00:00'));
		$this->assertFalse($this->hustler->isAfterEndTime());
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 04:00:01'));
		$this->assertTrue($this->hustler->isAfterEndTime());
	}

	public function testGetSecondsRemaining() {
		$this->deal->setTimeEnd(new \DateTime('2010-01-01 04:00:00'));
		$this->hustler->setDeal($this->deal);
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 03:00:00'));
		$remain1 = $this->deal->getTimeEnd()->getTimestamp() - $this->hustler->getTimeCurrent()->getTimestamp();
		$this->assertEquals($remain1, $this->hustler->getSecondsRemaining());
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 03:59:59'));
		$this->assertEquals(1, $this->hustler->getSecondsRemaining());
	}

	public function testGetMillisecondsRemaining() {
		$this->deal->setTimeEnd(new \DateTime('2010-01-01 04:00:00'));
		$this->hustler->setDeal($this->deal);
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 03:00:00'));
		$remain1 = ($this->deal->getTimeEnd()->getTimestamp() - $this->hustler->getTimeCurrent()->getTimestamp()) * 1000;
		$this->assertEquals($remain1, $this->hustler->getMillisecondsRemaining());
		$this->hustler->setTimeCurrent(new \DateTime('2010-01-01 03:59:59'));
		$this->assertEquals(1000, $this->hustler->getMillisecondsRemaining());
	}

	public function testIsInStock() {
		$max = 10;
		$sold1 = 5;
		$this->rule->setQtyMaxSold($max);
		$this->deal->setQtySold($sold1);
		$this->deal->setRule($this->rule);
		$this->hustler->setDeal($this->deal);
		$this->assertTrue($this->hustler->isInStock());
		$sold2 = 9;
		$this->deal->setQtySold($sold2);
		$this->assertTrue($this->hustler->isInStock());
		$sold3 = 10;
		$this->deal->setQtySold($sold3);
		$this->assertFalse($this->hustler->isInStock());
	}

	public function testIsThresholdMet() {
		$threshold = 10;
		$sold1 = 5;
		$this->rule->setQtyThreshold($threshold);
		$this->deal->setQtySold($sold1);
		$this->deal->setRule($this->rule);
		$this->hustler->setDeal($this->deal);
		$this->assertFalse($this->hustler->isThresholdMet());
		$sold2 = 9;
		$this->deal->setQtySold($sold2);
		$this->assertFalse($this->hustler->isThresholdMet());
		$sold3 = 10;
		$this->deal->setQtySold($sold3);
		$this->assertTrue($this->hustler->isThresholdMet());
		$sold4 = 11;
		$this->deal->setQtySold($sold4);
		$this->assertTrue($this->hustler->isThresholdMet());
	}

	public function testIsQtyRequestedInStock() {
		$max = 10;
		$sold1 = 5;
		$requested1 = 4;
		$this->rule->setQtyMaxSold($max);
		$this->deal->setQtySold($sold1);
		$this->deal->setRule($this->rule);
		$this->hustler->setDeal($this->deal);
		$this->hustler->setQtyRequested($requested1);
		$this->assertTrue($this->hustler->isQtyRequestedInStock());
		$requested2 = 5;
		$this->hustler->setQtyRequested($requested2);
		$this->assertTrue($this->hustler->isQtyRequestedInStock());
		$requested3 = 6;
		$this->hustler->setQtyRequested($requested3);
		$this->assertFalse($this->hustler->isQtyRequestedInStock());
	}

	public function testGetDealPrice() {
		$this->rule->setType(Rule::TYPE_PERCENT);
		$this->rule->setApply(25);
		$this->deal->setPrice(100);
		$this->deal->setRule($this->rule);
		$this->hustler->setDeal($this->deal);
		$this->assertEquals(75, $this->hustler->getDealPrice());
		$this->rule->setType(Rule::TYPE_REDUCED);
		$this->rule->setApply(35);
		$this->deal->setPrice(100);
		$this->assertEquals(65, $this->hustler->getDealPrice());	
	}
	
	public function testGetSavingsPercent() {
		$this->rule->setType(Rule::TYPE_PERCENT);
		$this->rule->setApply(45);	
		$this->deal->setPrice(100);
		$this->deal->setRule($this->rule);
		$this->hustler->setDeal($this->deal);
		$this->assertEquals(0.45, $this->hustler->getSavingsPercent());
		$this->rule->setType(Rule::TYPE_REDUCED);
		$this->rule->setApply(80);
		$this->assertEquals(0.80, $this->hustler->getSavingsPercent());
		$this->deal->setPrice(200);
		$this->assertEquals(0.40, $this->hustler->getSavingsPercent());
	}

	public function testGetSavingsReduced() {
		$this->rule->setType(Rule::TYPE_REDUCED);
		$this->rule->setApply(45);	
		$this->deal->setPrice(100);
		$this->deal->setRule($this->rule);
		$this->hustler->setDeal($this->deal);
		$this->assertEquals(45, $this->hustler->getSavingsReduced());
		$this->rule->setType(Rule::TYPE_PERCENT);
		$this->rule->setApply(35);
		$this->assertEquals(35, $this->hustler->getSavingsReduced());
		$this->deal->setPrice(200);
		$this->assertEquals(70, $this->hustler->getSavingsReduced());	
	}

}	

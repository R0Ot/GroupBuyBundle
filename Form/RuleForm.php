<?php

namespace Bundle\GroupBuyBundle\Form;

use Symfony\Component\Form;
use Bundle\GroupBuyBundle\Document\Rule;

class RuleForm extends Form\Form {

	protected function configure() {

		$this->add(new Form\ChoiceField('type', array('expanded' => false, 'choices' => Rule::getTypeChoices())));		
		$this->add(new Form\TextField('apply'));
		$this->add(new Form\TextField('qtyStart'));
		$this->add(new Form\TextField('qtyThreshold'));
		$this->add(new Form\TextField('qtyMaxSold'));

	}

}

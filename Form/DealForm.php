<?php

namespace Bundle\GroupBuyBundle\Form;

use Symfony\Component\Form;
use Bundle\GroupBuyBundle\Document\Rule;

class DealForm extends Form\Form {

	protected function configure() {		

		$this->add(new Form\CheckboxField('demo'));
		$this->add(new Form\TextField('title'));
		$this->add(new Form\TextareaField('description'));
		$this->add(new Form\DateTimeField('releaseTime', array('user_timezone' => 'America/New_York')));
		
		$this->add(new Form\DateTimeField('timeStart', array('user_timezone' => 'America/New_York')));
		$this->add(new Form\DateTimeField('timeEnd', array('user_timezone' => 'America/New_York')));
		$this->add(new Form\DateTimeField('timeExpire', array('user_timezone' => 'America/New_York')));
		
		$this->add(new Form\TextField('price'));
		$this->add(new Form\TextField('shippingCost'));

		$this->add(new Form\FieldGroup('ruleData'));
		$this['ruleData']->add(new Form\ChoiceField('type', array('expanded' => false, 'choices' => Rule::getTypeChoices())));		
		$this['ruleData']->add(new Form\TextField('apply'));
		$this['ruleData']->add(new Form\TextField('qtyStart'));
		$this['ruleData']->add(new Form\TextField('qtyThreshold'));
		$this['ruleData']->add(new Form\TextField('qtyMaxSold'));

		$this->add(new Form\FieldGroup('meta'));
		$this['meta']->add(new Form\TextField('title'));
		$this['meta']->add(new Form\TextField('description'));
		$this['meta']->add(new Form\TextField('keywords'));
	}

}

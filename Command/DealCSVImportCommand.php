<?php

namespace Bundle\GroupBuyBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Bundle\GroupBuyBundle\Document\Deal;
use Bundle\GroupBuyBundle\Document\Rule;

class DealCSVImportCommand extends Command {

	const ENCLOSURE = '"';
	const DELIMITER = ',';
	const CSV_INPUT_FILENAME  = 'deal_import.csv';

	protected $documentManager;
	protected $errors;

	protected function configure() {
		$this->setDefinition(array(
					new InputOption(
						'file-location',
						null,
						InputOption::PARAMETER_REQUIRED,
						'file-location is a required param'
						),
					))
			->setName('group-buy:deal:csv-import')
			->setDescription('Import Deals from CSV');
	}

	/**
	 * Performs parent init, also sets service instances for queue and
	 * event dispatcher if not set already
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 */
	protected function initialize(InputInterface $input, OutputInterface $output) {

		parent::initialize($input, $output);
		
		ini_set('auto_detect_line_endings', 1);
		
		$container = $this->container;

		if (!isset($this->documentManager)) {
			$this->setDocumentManager($container->get('doctrine.odm.mongodb.document_manager'));
		}

	}

	/**
	 * Gets events from queue @see $this->_queue, instantiates Event class
	 * instances and notifies the EventDisptacher @see $this->_dispatcher
	 *
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 */
	public function execute(InputInterface $input, OutputInterface $output) {

		$dm = $this->getDocumentManager();
		$fileLocation = $input->getOption('file-location');
		if ($fileLocation == 'default') {
			$source = __DIR__ . '/../Resources/files/';
		} else {
			$source = $fileLocation;
		}

		$sourceFile = $source.self::CSV_INPUT_FILENAME;

		$csvData = $this->getCsvData($sourceFile, $output);

		if (empty($csvData->body)) {
			throw new \Exception('Failed to load any data.');
		}

		$className = 'Deal';

		$output->writeln("Starting ".$className." import.......");

		$pass = array();
		$fail = array();
		$iteration = 0; // assuming first line of CSV is header and second is body
		foreach ($csvData->body as $row) {
			$line = $iteration + 2;
			$output->writeln('= = = = ROW START = = = =');
			try {
				$this->initPersist($row, $output);
				$dm->flush();
				$dm->clear();
				$output->writeln("DB flushed");				
				
			} catch(\Exception $ex ){
				$output->writeln("EXCEPTION: {$ex->getMessage()}");
			}
			$output->writeln('= = = = ROW END = = = =');
			$iteration++;
		}

	}

	protected function initPersist($row, $output) {
	
		$output->writeln("Preparing Deal");
		$deal = new Deal();
		$rule = new Rule();
		$deal->setRule($rule);

		if (isset($row['Deal[title]']) && trim($row['Deal[title]']) != '') {
			$deal->setTitle(trim($row['Deal[title]']));
		} 
		if (isset($row['Deal[description]']) && trim($row['Deal[description]']) != '') {
			$deal->setDescription(trim($row['Deal[description]']));
		}
		if (isset($row['Deal[meta][title]']) && trim($row['Deal[meta][title]']) != '') {
			$deal->setMetaTitle(trim($row['Deal[meta][title]']));
		} 
		if (isset($row['Deal[meta][description]']) && trim($row['Deal[meta][description]']) != '') {
			$deal->setMetaDescription(trim($row['Deal[meta][description]']));
		} 
		if (isset($row['Deal[meta][keywords]']) && trim($row['Deal[meta][keywords]']) != '') {
			$deal->setMetaKeywords(trim($row['Deal[meta][keywords]']));
		} 
		if (isset($row['Deal[releaseTime]']) && trim($row['Deal[releaseTime]']) != '') {
			$deal->setReleaseTime(new \DateTime(trim($row['Deal[releaseTime]'])));
		} 
		if (isset($row['Deal[timeStart]']) && trim($row['Deal[timeStart]']) != '') {
			$deal->setTimeStart(new \DateTime(trim($row['Deal[timeStart]'])));
		} 
		if (isset($row['Deal[timeEnd]']) && trim($row['Deal[timeEnd]']) != '') {
			$deal->setTimeEnd(new \DateTime(trim($row['Deal[timeEnd]'])));
		} 
		if (isset($row['Deal[timeExpire]']) && trim($row['Deal[timeExpire]']) != '') {
			$deal->setTimeExpire(new \DateTime(trim($row['Deal[timeExpire]'])));
		} 
		if (isset($row['Deal[price]']) && trim($row['Deal[price]']) != '') {
			$deal->setPrice(trim($row['Deal[price]']));
		} 
		if (isset($row['Deal[shippingCost]']) && trim($row['Deal[shippingCost]']) != '') {
			$deal->setShippingCost(trim($row['Deal[shippingCost]']));
		}
		if (isset($row['Deal[rule][type]']) && trim($row['Deal[rule][type]']) != '') {
			$rule->setType(trim($row['Deal[rule][type]']));
		}
		if (isset($row['Deal[rule][apply]']) && trim($row['Deal[rule][apply]']) != '') {
			$rule->setApply(trim($row['Deal[rule][apply]']));
		}
		if (isset($row['Deal[rule][qtyStart]']) && trim($row['Deal[rule][qtyStart]']) != '') {
			$rule->setQtyStart(trim($row['Deal[rule][qtyStart]']));
		}
		if (isset($row['Deal[rule][qtyThreshold]']) && trim($row['Deal[rule][qtyThreshold]']) != '') {
			$rule->setQtyThreshold(trim($row['Deal[rule][qtyThreshold]']));
		}
		if (isset($row['Deal[rule][qtyMaxSold]']) && trim($row['Deal[rule][qtyMaxSold]']) != '') {
			$rule->setQtyMaxSold(trim($row['Deal[rule][qtyMaxSold]']));
		}
		$this->getDocumentManager()->persist($deal);
		
	}

	protected function exportRows($fileName, $rows) {

		$fp = fopen($fileName, 'w');

		foreach ($rows as $row) {
			$this->writeRow($row, $fp);
		}

		fclose($fp);

		return $fileName;
	}

	protected function writeRow($row, $fp) {
		fputcsv($fp, $row, self::DELIMITER, self::ENCLOSURE);
	}

	protected function getCsvData($file, $output) {
		$returnData = new \stdClass();
		$returnData->head = array();
		$returnData->body = array();
		if (!file_exists($file)) {
			$output->writeln(sprintf('Unable to find CSV file: %s', $file));
			return $returnData;
		}

		$row = 1;

		if (($handle = fopen($file, "r")) !== FALSE) {
			while (($data = fgetcsv($handle, 5000, ",")) !== FALSE) {
				$num = count($data);
				if ($row == 1) {
					for ($c=0; $c < $num; $c++) {
						$returnData->head[$c] = $data[$c];
					}
				} else {
					$body = array();
					for ($c=0; $c < $num; $c++) {
						$key = $returnData->head[$c];
						$body[$key] = $data[$c];
					}
					$returnData->body[]	= $body;
				}
				$row++;
			}
			fclose($handle);
		}
		return $returnData;
	}

	public function setDocumentManager($documentManager) {
		$this->documentManager = $documentManager;
	}

	public function getDocumentManager() {
		return $this->documentManager;
	}

}
